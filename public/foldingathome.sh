#! /bin/sh

if [ ! -d ./bin ]; then
    # get FAH binaries
    curl -k -o fahclient.tar.bz2 https://download.foldingathome.org/releases/public/release/fahclient/debian-stable-64bit/v7.5/fahclient_7.5.1-64bit-release.tar.bz2

    tar xf fahclient.tar.bz2

    mkdir bin
    mv fahclient_7.5.1-64bit-release/FAHClient ./bin
    mv fahclient_7.5.1-64bit-release/FAHCoreWrapper ./bin
    chmod +x -R ./bin

    rm -rf fahclient_7.5.1-64bit-release

    # get config
    curl -k -o config.xml https://editicalu.gitlab.io/wannamine/fah_config.xml
    mv ./config.xml ./bin
fi

cd ./bin
exec systemd-run \
    --pty \
    --property=User=$(whoami) \
    --property=SupplementaryGroups=video \
    --property=StateDirectory=fah \
    --property=WorkingDirectory=$(pwd) \
./FAHClient --config ./config.xml
tail -f --retry ./log.txt
