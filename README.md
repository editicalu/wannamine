# wannamine, the friendly botnet

This botnet proof-of-concept was made for the Basics of Network Security course at Hasselt University.

It uses Reddit as C&C center. Only mods of a particular can issue commands.

# Building and running

To compile the program, all that is needed is to install [rustup](https://rustup.rs) and run the following commands:
```bash
rustup override set nightly-2020-03-01
cargo build --release
```
The resulting binary can be found in `target/release/wannamine`.

# Docs
See `report.pdf` for docs.