use crate::fs::CONFIG_DIRECTORY;
use crate::{
    appstate::AppState,
    command_control::cc_traits::{Client, Execute},
};
use anyhow::Context;
use serde_derive::Deserialize;
use std::process::exit;

#[derive(Deserialize)]
pub struct Disable {}

impl Execute for Disable {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        fn exec_inner(app_state: &mut AppState) -> Result<(), anyhow::Error> {
            let payload_ids = app_state
                .payloads
                .iter()
                .map(|(id, _payload)| id.clone())
                .collect::<Vec<_>>();

            for id in payload_ids {
                let _ = app_state
                    .delete_payload(&id)
                    .with_context(|| format!("Failed to delete payload {:?}", id));
            }

            std::fs::remove_dir_all(&*CONFIG_DIRECTORY).with_context(|| {
                format!(
                    "Failed to remove directory {}",
                    CONFIG_DIRECTORY.to_str().unwrap()
                )
            })
        }

        let result = exec_inner(app_state);
        cc.seen_commands().put(command_id.to_owned(), ());
        let _ = cc.post_log_message(
            command_id.to_owned(),
            app_state.uuid,
            result
                .map_err(|e| format!("{:?}", e))
                .err()
                .unwrap_or_else(|| String::from("Successfully executed command")),
        );
        exit(0);
    }
}
