use crate::command_control::cc_traits::{Client, Execute};
use crate::{appstate::AppState, fs::CONFIG_DIRECTORY};
use anyhow::Context;
use serde_derive::Deserialize;

#[derive(Deserialize)]
pub struct RemovePayload {
    id: String,
}

impl Execute for RemovePayload {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        fn exec_inner(
            instruction: &RemovePayload,
            app_state: &mut AppState,
        ) -> Result<(), anyhow::Error> {
            let dir = CONFIG_DIRECTORY.join(&instruction.id);

            std::fs::remove_dir_all(&dir)
                .with_context(|| format!("Failed to remove directory {}", dir.to_str().unwrap()))?;

            app_state.delete_payload(&instruction.id)
        }

        let result = exec_inner(self, app_state);
        cc.seen_commands().put(command_id.to_owned(), ());
        cc.post_log_message(
            command_id.to_owned(),
            app_state.uuid,
            result
                .map_err(|e| format!("{:?}", e))
                .err()
                .unwrap_or_else(|| String::from("Successfully executed command")),
        )
    }
}
