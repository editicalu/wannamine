use crate::{
    appstate::AppState,
    command_control::cc_traits::{Client, Execute},
};
use serde_derive::Deserialize;

/// Payload that doesn't do anything. This is used for things like Reddit posts that do not have an instruction in them, like the registration thread.
#[derive(Deserialize)]
pub struct AlertPayload {
    title: String,
    subtitle: Option<String>,
}

impl Execute for AlertPayload {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        let subtitle = match &self.subtitle {
            Some(sub) => Some(sub.as_str()),
            None => None,
        };

        let result = crate::alert::send_alert(&self.title, subtitle);

        cc.seen_commands().put(command_id.to_owned(), ());
        cc.post_log_message(
            command_id.to_owned(),
            app_state.uuid,
            result
                .map_err(|e| format!("{:?}", e))
                .err()
                .unwrap_or_else(|| String::from("Successfully executed command")),
        )
    }
}
