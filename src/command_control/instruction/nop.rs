use crate::{
    appstate::AppState,
    command_control::cc_traits::{Client, Execute},
};
use serde_derive::Deserialize;

/// Payload that doesn't do anything. This is used for things like Reddit posts that do not have an instruction in them, like the registration thread.
#[derive(Deserialize)]
pub struct NopPayload {}

impl Execute for NopPayload {
    fn execute(
        &self,
        _app_state: &mut AppState,
        _ccc: &mut dyn Client,
        _command_id: &str,
    ) -> Result<(), anyhow::Error> {
        Ok(())
    }
}
