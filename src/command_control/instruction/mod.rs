mod alert;
mod disable;
mod download_payload;
mod nop;
mod remove_payload;
mod start_payload;
mod status;
mod stop_payload;

use super::cc_traits::{Client, Execute};
use crate::appstate::AppState;
use anyhow::Context;
use anyhow::Error;

pub struct Instruction {
    executable: Box<dyn Execute>,
    uuids: Vec<uuid::Uuid>,
}

impl Instruction {
    pub fn new(executable: Box<dyn Execute>, uuids: Vec<uuid::Uuid>) -> Self {
        Instruction { executable, uuids }
    }

    pub fn uuids(&self) -> &Vec<uuid::Uuid> {
        &self.uuids
    }
}

impl Execute for Instruction {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), Error> {
        self.executable.execute(app_state, cc, command_id)
    }
}

#[derive(Deserialize)]
struct InstrMain {
    clients: Option<Vec<String>>,
    download_payload: Option<download_payload::DownloadPayload>,
    remove_payload: Option<remove_payload::RemovePayload>,
    start_payload: Option<start_payload::StartPayload>,
    stop_payload: Option<stop_payload::StopPayload>,
    alert: Option<alert::AlertPayload>,
    disable: Option<disable::Disable>,
    status: Option<status::Status>,
    nop: Option<nop::NopPayload>,
}

pub fn parse(message: &str) -> Result<Instruction, Error> {
    let toml: InstrMain = toml::from_str(message)
        .map_err(|e| anyhow!("Failed to parse message as TOML\n\n{:?}", e))?;
    let uuids = {
        if let Some(clients) = toml.clients {
            clients
                .into_iter()
                .map(|uuid| uuid::Uuid::parse_str(&uuid))
                .collect::<Result<Vec<uuid::Uuid>, _>>()
                .context("Failed to parse UUIDs")?
        } else {
            Vec::new()
        }
    };

    if let Some(instr) = toml.download_payload {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.remove_payload {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.start_payload {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.stop_payload {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.disable {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.alert {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.status {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else if let Some(instr) = toml.nop {
        Ok(Instruction::new(Box::new(instr), uuids))
    } else {
        Err(anyhow!("No recognizable command provided"))
    }
}
