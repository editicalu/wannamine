use crate::{
    appstate::AppState,
    command_control::cc_traits::{Client, Execute},
};
use anyhow::Context;
use serde_derive::Deserialize;
#[derive(Deserialize)]
pub struct StopPayload {
    id: String,
}

impl Execute for StopPayload {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        fn exec_inner(
            instruction: &StopPayload,
            app_state: &mut AppState,
        ) -> Result<(), anyhow::Error> {
            app_state
                .stop_payload(&instruction.id)
                .with_context(|| format!("Failed to kill payload {}", &instruction.id))
        }

        let result = exec_inner(self, app_state);
        cc.seen_commands().put(command_id.to_owned(), ());
        cc.post_log_message(
            command_id.to_owned(),
            app_state.uuid,
            result
                .map_err(|e| format!("{:?}", e))
                .err()
                .unwrap_or_else(|| String::from("Successfully executed command")),
        )
    }
}
