use crate::{
    appstate::AppState,
    command_control::cc_traits::{Client, Execute},
};
use anyhow::Context;
use serde_derive::Deserialize;

#[derive(Deserialize)]
pub struct StartPayload {
    id: String,
    args: Vec<String>,
}

impl Execute for StartPayload {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        fn exec_inner(
            instruction: &StartPayload,
            app_state: &mut AppState,
        ) -> Result<(), anyhow::Error> {
            app_state
                .update_args(&instruction.id, instruction.args.clone())
                .with_context(|| {
                    format!("Failed to update arguments for payload {}", instruction.id)
                })?;

            app_state
                .start_payload(&instruction.id)
                .with_context(|| format!("Failed to spawn payload {}", &instruction.id))
        }

        let result = exec_inner(self, app_state);
        cc.seen_commands().put(command_id.to_owned(), ());
        cc.post_log_message(
            command_id.to_owned(),
            app_state.uuid,
            result
                .map_err(|e| format!("{:?}", e))
                .err()
                .unwrap_or_else(|| String::from("Successfully executed command")),
        )
    }
}
