use crate::{
    appstate::AppState,
    command_control::cc_traits::{Client, Execute},
};
use anyhow::Context;
use serde_derive::Deserialize;

#[derive(Deserialize)]
pub struct Status {}

impl Execute for Status {
    fn execute(
        &self,
        app_state: &mut AppState,
        cc: &mut dyn Client,
        command_id: &str,
    ) -> Result<(), anyhow::Error> {
        let result = app_state
            .payloads
            .iter_mut()
            .map(|(_id, payload)| payload.fmt_status())
            .collect::<Result<Vec<String>, _>>()
            .context("Failed to format payload statuses")?
            .join("\n\n-----------------------------\n\n");
        cc.seen_commands().put(command_id.to_owned(), ());
        cc.post_log_message(command_id.to_owned(), app_state.uuid, result)
    }
}
