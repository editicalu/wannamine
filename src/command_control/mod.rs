pub mod cc_traits;
mod instruction;
pub mod reddit;

use anyhow::{Context, Error};
use std::fmt::Debug;

pub struct CommandMessage {
    pub id: String,
    author: String,
    title: String,
    pub instruction: instruction::Instruction,
}

impl CommandMessage {
    fn new(id: String, author: String, title: String, text: String) -> Result<Self, Error> {
        Ok(CommandMessage {
            id,
            author,
            title,
            instruction: instruction::parse(&text).context("Failed to parse instruction")?,
        })
    }
}

impl Debug for CommandMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("CommandMessage")
            .field("id", &self.id)
            .field("author", &self.author)
            .field("title", &self.title)
            .finish()
    }
}
