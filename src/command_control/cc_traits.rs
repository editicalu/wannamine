use super::CommandMessage;
use crate::appstate::AppState;
use anyhow::Error;

pub trait Client {
    fn seen_commands(&mut self) -> &mut lru::LruCache<String, ()>;
    fn newest_commands(&self, uuid: uuid::Uuid) -> Result<Vec<CommandMessage>, Error>;
    fn post_log_message(
        &self,
        command_id: String,
        uuid: uuid::Uuid,
        log_msg: String,
    ) -> Result<(), Error>;
    fn register(&self, uuid: uuid::Uuid, os_data: crate::os::OsSummary) -> Result<(), Error>;
    fn set_seen_commands(&mut self, seen: Vec<String>);
}

pub trait Execute {
    fn execute(&self, app_state: &mut AppState, cc: &mut dyn Client, id: &str)
        -> Result<(), Error>;
}
