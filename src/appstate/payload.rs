use crate::fs::CONFIG_DIRECTORY;
use anyhow::{Context, Result};
use std::io::Read;
use std::path::PathBuf;
use std::process::{Child, Command, Stdio};

const BUFF_SIZE: usize = 128;

#[derive(Serialize, Deserialize)]
pub(super) struct SavedPayLoad {
    pub(super) args: Vec<String>,
    pub(super) running: bool,
}

pub enum PayLoadStatus {
    Running(Child),
    Stopped,
}

impl PayLoadStatus {
    pub fn running(&self) -> bool {
        match self {
            Self::Running(_) => true,
            _ => false,
        }
    }
}

pub struct PayLoad {
    path: std::path::PathBuf,
    pub(super) status: PayLoadStatus,
    pub(super) args: Vec<String>,
}

impl PayLoad {
    pub fn new(name: &str, args: Vec<String>) -> Result<Self> {
        let executable: PathBuf = CONFIG_DIRECTORY.join(name).join(name);

        if !executable.is_file() {
            Err(anyhow!("payload not found"))
        } else {
            Ok(Self {
                status: PayLoadStatus::Stopped,
                path: executable,
                args,
            })
        }
    }

    pub fn start(&mut self) -> Result<()> {
        match &self.status {
            PayLoadStatus::Stopped => {
                self.status = PayLoadStatus::Running(
                    Command::new("bash")
                        .arg("-c")
                        .arg(&self.path)
                        .args(&self.args)
                        .stdout(Stdio::piped())
                        .current_dir(self.path.parent().context("No workdir for payload")?)
                        .spawn()?,
                );
            }

            PayLoadStatus::Running(_) => {}
        };
        Ok(())
    }

    pub fn stop(&mut self) -> Result<()> {
        if let PayLoadStatus::Running(process) = &mut self.status {
            process.kill()?;
            self.status = PayLoadStatus::Stopped;
        }
        Ok(())
    }

    pub fn fmt_status(&mut self) -> Result<String> {
        let status = match self.status {
            PayLoadStatus::Running(_) => "Running",
            PayLoadStatus::Stopped => "Stopped",
        };
        let mut status = format!(
            "{}: {} {}",
            status,
            self.path
                .to_str()
                .with_context(|| format!("Payload path is an invalid string: {:?}", self.path))?,
            self.args.join(" ")
        );

        match &mut self.status {
            PayLoadStatus::Running(process) => {
                let mut buffer = [0; 5000]; // Reddit has a char comment limit
                let path = &self.path;

                let file_handle = process
                    .stdout
                    .as_mut()
                    .ok_or_else(|| anyhow!("Payload has no stdout: {:?}", path))?;

                let mut read_bytes = BUFF_SIZE;
                while read_bytes >= BUFF_SIZE {
                    read_bytes = file_handle.read(&mut buffer).with_context(|| {
                        format!("Failed to read payload process' stdout: {:?}", path)
                    })?;
                }

                status = format!(
                    "{}\n\n{}",
                    status,
                    String::from_utf8((&buffer).to_vec()).unwrap()
                );
            }

            PayLoadStatus::Stopped => {}
        }

        Ok(status)
    }
}
