//! Module to get data from the operating system. Currently, only Linux is supported by this module.

use anyhow::{Context, Result};
use std::net::{Ipv4Addr, Ipv6Addr};

#[derive(Debug)]
pub struct OsSummary {
    pub hostname: String,
    // cpu_info: String,
    pub cpu_cores: usize,
    pub gpu_count: usize,
    pub ipv4_addr: Option<Ipv4Addr>,
    pub ipv6_addr: Option<Ipv6Addr>,
}

pub fn get_os_summary() -> Result<OsSummary> {
    use regex::Regex;

    let cpu_info =
        std::fs::read_to_string("/proc/cpuinfo").context("Failed to read /proc/cpuinfo")?;

    let cpu_cores = Regex::new(r"processor\s+:\s+\d+")
        .unwrap()
        .captures_iter(&cpu_info)
        .count();

    let gpu_count: usize = std::process::Command::new("lspci")
        .output()
        .context("Failed to get output from lspci")?
        .stdout
        .split(|byte| *byte == b'\n')
        .map(|line| String::from_utf8(line.to_vec()).unwrap())
        .filter(|line| line.contains(" VGA "))
        .count();

    let hostname = String::from(
        std::fs::read_to_string("/etc/hostname")
            .context("Failed to read /etc/hostname")?
            .lines()
            .next()
            .unwrap(),
    );

    let ipv4_addr = {
        #[derive(Deserialize)]
        struct Ip {
            ip: String,
        }

        reqwest::blocking::get("https://api.ipify.org?format=json")
            .map_err(|_| ())
            .and_then(|item| item.json::<Ip>().map_err(|_| ()))
            .and_then(|item| item.ip.parse().map_err(|_| ()))
            .ok()
    };

    let ipv6_addr: Option<Ipv6Addr> = {
        #[derive(Deserialize)]
        struct Ip {
            ip: String,
        }

        reqwest::blocking::get("https://api6.ipify.org?format=json")
            .map_err(|_| ())
            .and_then(|item| item.json::<Ip>().map_err(|_| ()))
            .and_then(|item| item.ip.parse().map_err(|_| ()))
            .ok()
    };

    Ok(OsSummary {
        hostname,
        // cpu_info,
        cpu_cores,
        gpu_count,
        ipv4_addr,
        ipv6_addr,
    })
}
