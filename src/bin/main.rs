extern crate wannamine;

use anyhow::Error;

fn main() -> Result<(), Error> {
    dotenv::dotenv()?;
    wannamine::show_main_alert()?;

    loop {
        let main = wannamine::main();

        if main.is_ok() {
            return Ok(());
        } else {
            eprintln!("{:?}", main);
        }
        std::thread::sleep(std::time::Duration::from_secs(3))
    }
}
