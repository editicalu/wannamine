use std::path::PathBuf;

lazy_static! {
    pub static ref CONFIG_DIRECTORY: PathBuf = dirs::config_dir()
        .expect("no config directory found")
        .join("wannamine");
    pub static ref CONFIG_FILE: PathBuf = CONFIG_DIRECTORY.join("config.toml");
}
