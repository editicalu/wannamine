use anyhow::{Context, Result};
use notify_rust::Notification;

pub fn send_alert(text: &str, subtitle: Option<&str>) -> Result<()> {
    let mut notification = Notification::new();
    if let Some(subtitle) = subtitle {
        notification.summary(text).body(subtitle)
    } else {
        notification.summary(text)
    }
    .show()
    .map(|_| ())
    .context("Could not show notification")
}

pub fn show_main_alert() -> Result<()> {
    send_alert(
        "Wannamine Botnet Active",
        Some("To stop the botnet, run `killall wannamine`."),
    )
}
